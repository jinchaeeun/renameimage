﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace RenameImage
{

    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        //파일 존재 여부 체크 메서드
        private static bool FileExistsCheck(string strFilePath, string strOldFile)
        {
            strOldFile = strFilePath + "\\" + strOldFile;
            if (System.IO.File.Exists(strOldFile))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        //============================================================================
        //생성 버튼 클릭
        private void BtnSave_Click(object sender, EventArgs e)
        {     
            string strId = itemSelectedNumber; //순서 선택

            string strItem_dn = itemSelectedDN; //d, n 선택

            string strName = filename.Text; //파일명

            string strCreationDate = CreationDate.Value.ToShortDateString(); //생성일자
            string strDate = expiryDate.Value.ToShortDateString(); //유효일자

            string strFix_s = s.Text; //s 문자

            string strSite = url.Text; //연결 사이트 주소

            string strLineAddRgb = "#FF" + lineAddColor.Text;   //줄 광고 색상
            string strFrontAddRgb = "#FF" + frontAddColor.Text; //전면 광고 색상
            string strLockAddRgb = "#FF" + lockAddColor.Text;   //잠금 광고 색상

            string strPrice = price.Text; //가격

            //공백을 제거한 문자열 길이
            int LineRgblength = strLineAddRgb.Trim().Length;
            int FrontRgblength = strFrontAddRgb.Trim().Length;
            int LockRgblength = strLockAddRgb.Trim().Length;

            //============================================================================
            //빈칸 예외처리 (빈칸 있으면 메시지 창 띄우고 수행 안함)
            //광고 색상 예외처리 6자리 수인지 확인하기
            if (strId.Equals(""))
            {
                MessageBox.Show("순서를 선택하십시오.");
                return;
            }
            else if (strItem_dn.Equals(""))
            {
                MessageBox.Show("종류를 선택하십시오.");
                return;
            }
            else if (strName.Equals(""))
            {
                MessageBox.Show("파일명을 입력하십시오.");
                filename.Focus();
                return;
            }
            else if (strSite.Equals(""))
            {
                MessageBox.Show("접속 사이트 주소를 입력하십시오.");
                url.Focus();
                return;
            }
            else if (strLineAddRgb.Equals("") || !(LineRgblength == 9))
            {
                MessageBox.Show("줄 광고 색상 6자리를 입력하세요\n(RGB값 입력 ex)112233)");
                lineAddColor.Focus();
                return;
            }
            else if (strFrontAddRgb.Equals("") || !(FrontRgblength == 9))
            {
                MessageBox.Show("전면 광고 색상 6자리를 입력하세요\n(RGB값 입력 ex)112233)");
                frontAddColor.Focus();
                return;
            }

            else if (strLockAddRgb.Equals("") || !(LockRgblength == 9))
            {
                MessageBox.Show("잠금 화면 광고 색상 6자리를 입력하세요\n(RGB값 입력 ex)112233)");
                lockAddColor.Focus();
                return;
            }
            else if (strPrice.Equals(""))
            {
                MessageBox.Show("가격을 입력하십시오.");
                price.Focus();
                return;
            }
            //============================================================================
            //이미지 파일 예외처리 (1 / 2 / 3 / 4.png)

            //파일 주소 설정
            string strFilePath = @"C:\bin\orig\";
            string[] strOldFile = new string[] { "1.png", "2.png", "3.png", "4.png" };
            for (int i = 0; i < 4; i++)
            {
                bool check = FileExistsCheck(strFilePath, strOldFile[i]); //파일 경로
                if (check.Equals(false)) //파일 미존재
                {
                    MessageBox.Show(strFilePath + strOldFile[i] + " 파일이 없습니다.");
                    return; //종료
                }
            }

            //============================================================================
            // 각 필드를 세미콜론으로 결합한다.
            string strData = strId + ";" + strItem_dn + ";" + strName + ";" + strCreationDate + ";" + strDate + ";" + strFix_s + ";" + strSite + ";" + strLineAddRgb + ";" + strFrontAddRgb + ";" + strLockAddRgb + ";" + strPrice + ";";

            //============================================================================

            //폴더 없으면 생성
            string strCFolderPath = @"C:\bin\trans\"+strId;
            DirectoryInfo di = new DirectoryInfo(strCFolderPath);
            if (!System.IO.Directory.Exists(strCFolderPath))
            {
                MessageBox.Show(strCFolderPath + "경로를 생성합니다.");
                di.Create();
            }
            //폴더의 파일 모두 삭제
            else
            {
                MessageBox.Show("폴더의 모든 파일 (기존의 chk_" + strId + ".jpg 파일 및 이미지.png) 삭제 후 재생성합니다");
                string[] files = System.IO.Directory.GetFiles(@"C:\bin\trans\" + strId);
                foreach(string s in files)
                {
                    string fileName = System.IO.Path.GetFileName(s);                   
                    string deletefile = @"C:\bin\trans\" + strId + "//" + fileName;
                    System.IO.File.Delete(deletefile);
                }
            }

            //============================================================================
            //파일 생성
            //텍스트 파일을 쓸 때 StreamWriter를 사용함
            StreamWriter wr = new StreamWriter(@"C:\bin\trans\" + strId + "//" + "chk_" + strId + ".jpg", true); // true는 뒤에 추가하기, false는 덮어쓰기인데 에러날 수 있어서 위의 파일 삭제하는 방법을 선호.
            wr.WriteLine(strData);  // 한 라인을 저장
            wr.Close();

            //기존 이미지 파일 복사 이름 변경
            for (int i = 0; i < 4; i++)
            {
                File.Copy(@"C:\bin\orig\" + strOldFile[i], @"C:\bin\trans\"+ strId + "//" + strName + "_" + strOldFile[i]); //기존 이미지 파일, 변경할 이미지 파일 이름
            }
            MessageBox.Show("저장하였습니다!");
        }

        //============================================================================        
        //선택한 item의 index와 내용 가져오기
        
        //순서
        private void comboBoxNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxNumber.SelectedIndex >= 0)
            {
               itemSelectedNumber = comboBoxNumber.SelectedItem as string;
                
                string strNum = itemSelectedNumber;
                string strSelDate = CreationDate.Value.ToShortDateString() as string;  //CreationDate 값 가져오기
                string strMm= strSelDate.Substring(5, 2);                             //문자열 추출 (월만)
                string strDd = strSelDate.Substring(8, 2);                            //문자열 추출 (일만)
                filename.Text = strNum + "_" + strMm + strDd;
                
            }
        }
        private string itemSelectedNumber;

        //종류
        private void comboBoxdn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxdn.SelectedIndex >= 0)
            {
                this.itemSelectedDN = comboBoxdn.SelectedItem as string;
            }
        }
        private string itemSelectedDN;
        
        //생성일자
        private void CreationDate_ValueChanged(object sender, EventArgs e)
        {
            this.SelectDate = CreationDate.Value.ToShortDateString() as string;
            string m = this.SelectDate.Substring(5, 2);
            string d = this.SelectDate.Substring(8, 2);

            string strSubMMDD = m + d;
            string strNum = itemSelectedNumber;

            filename.Text = strNum + "_" + strSubMMDD;
        }
        private string SelectDate;

        //=-=-=-=-=-=-
        //winForm load되어질 때 콤보박스 아이템 초기화 및 선택할 수 있는 가장 작은 날짜 지정
        private void MainForm_Load(object sender, EventArgs e)
        {
            //winForm 이름
            this.Text = "Rename Image File [2020.01.15]";
            //순서 아이템 추가
            string[] strNumber=new string[10];
            for (int i=0; i<10; i++)
            {
                strNumber[i] = "" + (i+1) + "";
            }
            comboBoxNumber.Items.AddRange(strNumber);
            comboBoxNumber.SelectedIndex = 0;

            //적용방법 아이템 추가
            string[] strDN = { "d", "n" };
            //d,n 선택 콤보박스 데이터 초기화
            comboBoxdn.Items.AddRange(strDN);
            //첫번째 아이템으로 기본 선택값 지정
            comboBoxdn.SelectedIndex = 0;

            //선택할 수 있는 날짜의 최솟값 = 오늘
            expiryDate.MinDate = DateTime.Now;

            this.lblColor1.Text = "색상 1\n(줄 광고)";
            this.lblColor2.Text = "색상 2\n(전면 광고)";
            this.lblColor3.Text = "색상 3\n(잠금 화면 광고)";
        }
       
        //============================================================================
        //미리보기 버튼 클릭 시
        private void button1_Click(object sender, EventArgs e)
        {            
            string strId = itemSelectedNumber; //순서 선택

            string strItem_dn = itemSelectedDN; //d, n 선택

            string strName = filename.Text; //파일명

            string strCreationDate = CreationDate.Value.ToShortDateString(); //생성일자
            string strDate = expiryDate.Value.ToShortDateString(); //유효일자

            string strFix_s = s.Text; //s 문자

            string strSite = url.Text; //연결 사이트 주소

            string strLineAddRgb = "#FF" + lineAddColor.Text; //줄 광고 색상
            string strFrontAddRgb = "#FF" + frontAddColor.Text; //색상
            string strLockAddRgb = "#FF" + lockAddColor.Text; //색상

            string strPrice = price.Text; //가격

            //공백을 제거한 문자열 길이
            int LineRgblength = strLineAddRgb.Trim().Length;
            int FrontRgblength = strFrontAddRgb.Trim().Length;
            int LockRgblength = strLockAddRgb.Trim().Length;

            //============================================================================
            //빈칸 예외처리 (빈칸 있으면 메시지 창 띄우고 수행 안함)
            //광고 색상 예외처리 6자리 수인지 확인하기
            if (strId.Equals(""))
            {
                MessageBox.Show("순서를 선택하십시오.");
                return;
            }
            else if (strItem_dn.Equals(""))
            {
                MessageBox.Show("종류를 선택하십시오.");
                return;
            }
            else if (strName.Equals(""))
            {
                MessageBox.Show("파일명을 입력하십시오.");
                filename.Focus();
                return;
            }
            else if (strSite.Equals(""))
            {
                MessageBox.Show("접속 사이트 주소를 입력하십시오.");
                url.Focus();
                return;
            }
            else if (strLineAddRgb.Equals("") || !(LineRgblength == 9))
            {
                MessageBox.Show("줄 광고 색상 6자리를 입력하세요\n(RGB값 입력 ex)112233)");
                lineAddColor.Focus();
                return;
            }
            else if (strFrontAddRgb.Equals("") || !(FrontRgblength == 9))
            {
                MessageBox.Show("전면 광고 색상 6자리를 입력하세요\n(RGB값 입력 ex)112233)");
                frontAddColor.Focus();
                return;
            }

            else if (strLockAddRgb.Equals("") || !(LockRgblength == 9))
            {
                MessageBox.Show("잠금 화면 광고 색상 6자리를 입력하세요\n(RGB값 입력 ex)112233)");
                lockAddColor.Focus();
                return;
            }
            else if (strPrice.Equals(""))
            {
                MessageBox.Show("가격을 입력하십시오.");
                price.Focus();
                return;
            }

            //============================================================================
            //TextBox에 출력
            string strData = strId + ";" + strItem_dn + ";" + strName + ";" + strCreationDate + ";" + strDate + ";" + strFix_s + ";" + strSite + ";" + strLineAddRgb + ";" + strFrontAddRgb + ";" + strLockAddRgb + ";" + strPrice + ";";
            previewText.Text = strData;
        }

        //========================================================================
        //컨트롤이 폼의 활성 컨트롤이 아닐 때 발생하는 이벤트 (광고 테두리 색상 입력)
        
        //줄 광고
        private void lineAddColor_Leave(object sender, EventArgs e)
        {
            string strAddRgb = "FF" + lineAddColor.Text;   //줄 광고 색상
            int nRet = colorhextodecimal(strAddRgb);

            if (nRet == -1)
            {
                lineAddColor.Focus();
                return;
            }

            this.pBoxLineColor.BackColor = System.Drawing.Color.FromArgb(nRet);
        }
        //전면 광고
        private void frontAddColor_Leave(object sender, EventArgs e)
        {
            string strAddRgb = "FF" + frontAddColor.Text;   //전면 광고 색상
            int nRet = colorhextodecimal(strAddRgb);

            if (nRet == -1)
            {
                frontAddColor.Focus();
                return;
            }

            //pictureBox에 색상 출력
            this.pBoxFrontColor.BackColor = System.Drawing.Color.FromArgb(nRet);        
        }

        //잠금 화면 광고
        private void lockAddColor_Leave(object sender, EventArgs e)
        {
            string strAddRgb = "FF" + lockAddColor.Text;   //잠금 화면 광고 색상
            int nRet = colorhextodecimal(strAddRgb);

            if (nRet == -1)
            {
                lockAddColor.Focus();
                return;
            }

            //pictureBox에 색상 출력
            this.pBoxLockColor.BackColor = System.Drawing.Color.FromArgb(nRet);
        }

        //======================================================
        //입력 받은 16진수 10진수로 변경
        public int colorhextodecimal(string strAddRgb)
        {
            int iDec = 0;
            
            //=======================================================
            //문자열 길이 체크 FF+입력한 문자열
            if (strAddRgb.Length != 8)
            {
                MessageBox.Show("16진수 6자리를 입력하여 주십시오.\n(A~F사이의 문자 또는 0~9 사이의 숫자 입력)");
                return -1;
            }

            //====================================================
            try
            {
                //16진수 문자열을 10진수로 변환
                iDec = Convert.ToInt32(strAddRgb, 16);
                return iDec;
            }
            catch (Exception e)
            {
                //에러 처리, 로깅 등
                MessageBox.Show("A~F사이의 문자 또는 0~9 사이의 숫자만 입력 가능합니다!");
                return -1;
            }
        }
       
    }
}
