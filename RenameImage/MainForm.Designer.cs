﻿namespace RenameImage
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.filename = new System.Windows.Forms.TextBox();
            this.s = new System.Windows.Forms.TextBox();
            this.url = new System.Windows.Forms.TextBox();
            this.lineAddColor = new System.Windows.Forms.TextBox();
            this.price = new System.Windows.Forms.TextBox();
            this.comboBoxdn = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblColor1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.expiryDate = new System.Windows.Forms.DateTimePicker();
            this.lblColor2 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.lblColor3 = new System.Windows.Forms.Label();
            this.frontAddColor = new System.Windows.Forms.TextBox();
            this.lockAddColor = new System.Windows.Forms.TextBox();
            this.previewText = new System.Windows.Forms.TextBox();
            this.previewLabel = new System.Windows.Forms.Label();
            this.BtnPreview = new System.Windows.Forms.Button();
            this.comboBoxNumber = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CreationDate = new System.Windows.Forms.DateTimePicker();
            this.pBoxLineColor = new System.Windows.Forms.PictureBox();
            this.pBoxFrontColor = new System.Windows.Forms.PictureBox();
            this.pBoxLockColor = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLineColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxFrontColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLockColor)).BeginInit();
            this.SuspendLayout();
            // 
            // filename
            // 
            this.filename.BackColor = System.Drawing.SystemColors.Window;
            this.filename.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.filename.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.filename.Location = new System.Drawing.Point(596, 17);
            this.filename.Margin = new System.Windows.Forms.Padding(6, 5, 20, 5);
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(291, 17);
            this.filename.TabIndex = 2;
            // 
            // s
            // 
            this.s.BackColor = System.Drawing.SystemColors.Window;
            this.s.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.s.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.s.Location = new System.Drawing.Point(98, 84);
            this.s.Margin = new System.Windows.Forms.Padding(6, 15, 20, 5);
            this.s.Name = "s";
            this.s.ReadOnly = true;
            this.s.Size = new System.Drawing.Size(25, 17);
            this.s.TabIndex = 6;
            this.s.Text = "s";
            this.s.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // url
            // 
            this.url.BackColor = System.Drawing.SystemColors.Window;
            this.url.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.url.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.url.Location = new System.Drawing.Point(336, 84);
            this.url.Margin = new System.Windows.Forms.Padding(6, 21, 6, 5);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(552, 17);
            this.url.TabIndex = 7;
            // 
            // lineAddColor
            // 
            this.lineAddColor.BackColor = System.Drawing.SystemColors.Window;
            this.lineAddColor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lineAddColor.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lineAddColor.Location = new System.Drawing.Point(145, 124);
            this.lineAddColor.Margin = new System.Windows.Forms.Padding(6, 5, 20, 5);
            this.lineAddColor.Name = "lineAddColor";
            this.lineAddColor.Size = new System.Drawing.Size(135, 17);
            this.lineAddColor.TabIndex = 8;
            this.lineAddColor.Leave += new System.EventHandler(this.lineAddColor_Leave);
            // 
            // price
            // 
            this.price.BackColor = System.Drawing.SystemColors.Window;
            this.price.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.price.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.price.Location = new System.Drawing.Point(145, 188);
            this.price.Margin = new System.Windows.Forms.Padding(6, 21, 6, 5);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(135, 17);
            this.price.TabIndex = 11;
            // 
            // comboBoxdn
            // 
            this.comboBoxdn.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxdn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxdn.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxdn.FormattingEnabled = true;
            this.comboBoxdn.Location = new System.Drawing.Point(369, 15);
            this.comboBoxdn.Margin = new System.Windows.Forms.Padding(6, 5, 20, 5);
            this.comboBoxdn.Name = "comboBoxdn";
            this.comboBoxdn.Size = new System.Drawing.Size(70, 22);
            this.comboBoxdn.TabIndex = 1;
            this.comboBoxdn.SelectedIndexChanged += new System.EventHandler(this.comboBoxdn_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(26, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 5, 20, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 23);
            this.label1.TabIndex = 20;
            this.label1.Text = "순서";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(248, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 5, 20, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 23);
            this.label2.TabIndex = 21;
            this.label2.Text = "적용 방법";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(474, 15);
            this.label3.Margin = new System.Windows.Forms.Padding(10, 5, 20, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 23);
            this.label3.TabIndex = 22;
            this.label3.Text = "이미지명";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(475, 47);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 21, 20, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 28);
            this.label4.TabIndex = 24;
            this.label4.Text = "유효일자";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.label5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label5.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(27, 84);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 15, 20, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "종류";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(149, 84);
            this.label6.Margin = new System.Windows.Forms.Padding(10, 5, 20, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 21);
            this.label6.TabIndex = 26;
            this.label6.Text = "접속 사이트 주소";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblColor1
            // 
            this.lblColor1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblColor1.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblColor1.Location = new System.Drawing.Point(27, 116);
            this.lblColor1.Margin = new System.Windows.Forms.Padding(6, 15, 20, 5);
            this.lblColor1.Name = "lblColor1";
            this.lblColor1.Size = new System.Drawing.Size(96, 34);
            this.lblColor1.TabIndex = 27;
            this.lblColor1.Text = "색상 1 (줄 광고) ";
            this.lblColor1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label8.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(27, 188);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 15, 20, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 18);
            this.label8.TabIndex = 30;
            this.label8.Text = "단가";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // expiryDate
            // 
            this.expiryDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.expiryDate.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.expiryDate.Location = new System.Drawing.Point(597, 47);
            this.expiryDate.Margin = new System.Windows.Forms.Padding(6, 21, 6, 5);
            this.expiryDate.MinDate = new System.DateTime(2020, 1, 6, 0, 0, 0, 0);
            this.expiryDate.Name = "expiryDate";
            this.expiryDate.Size = new System.Drawing.Size(291, 24);
            this.expiryDate.TabIndex = 5;
            this.expiryDate.Value = new System.DateTime(2020, 1, 10, 0, 0, 0, 0);
            // 
            // lblColor2
            // 
            this.lblColor2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblColor2.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblColor2.Location = new System.Drawing.Point(305, 116);
            this.lblColor2.Margin = new System.Windows.Forms.Padding(10, 5, 20, 5);
            this.lblColor2.Name = "lblColor2";
            this.lblColor2.Size = new System.Drawing.Size(96, 34);
            this.lblColor2.TabIndex = 28;
            this.lblColor2.Text = "색상 2 (전면 광고)";
            this.lblColor2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnSave
            // 
            this.BtnSave.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BtnSave.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BtnSave.Location = new System.Drawing.Point(503, 255);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(14, 21, 4, 3);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(385, 27);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "생성";
            this.BtnSave.UseVisualStyleBackColor = false;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblColor3
            // 
            this.lblColor3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblColor3.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblColor3.Location = new System.Drawing.Point(592, 116);
            this.lblColor3.Margin = new System.Windows.Forms.Padding(10, 5, 20, 5);
            this.lblColor3.Name = "lblColor3";
            this.lblColor3.Size = new System.Drawing.Size(134, 34);
            this.lblColor3.TabIndex = 29;
            this.lblColor3.Text = "색상 3 (잠금 화면 광고)";
            this.lblColor3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frontAddColor
            // 
            this.frontAddColor.BackColor = System.Drawing.SystemColors.Window;
            this.frontAddColor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.frontAddColor.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.frontAddColor.Location = new System.Drawing.Point(427, 124);
            this.frontAddColor.Margin = new System.Windows.Forms.Padding(6, 21, 20, 5);
            this.frontAddColor.Name = "frontAddColor";
            this.frontAddColor.Size = new System.Drawing.Size(135, 17);
            this.frontAddColor.TabIndex = 9;
            this.frontAddColor.Leave += new System.EventHandler(this.frontAddColor_Leave);
            // 
            // lockAddColor
            // 
            this.lockAddColor.BackColor = System.Drawing.SystemColors.Window;
            this.lockAddColor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lockAddColor.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lockAddColor.Location = new System.Drawing.Point(752, 124);
            this.lockAddColor.Margin = new System.Windows.Forms.Padding(6, 21, 6, 5);
            this.lockAddColor.Name = "lockAddColor";
            this.lockAddColor.Size = new System.Drawing.Size(135, 17);
            this.lockAddColor.TabIndex = 10;
            this.lockAddColor.Leave += new System.EventHandler(this.lockAddColor_Leave);
            // 
            // previewText
            // 
            this.previewText.BackColor = System.Drawing.SystemColors.Window;
            this.previewText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.previewText.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.previewText.Location = new System.Drawing.Point(145, 220);
            this.previewText.Margin = new System.Windows.Forms.Padding(6, 21, 6, 5);
            this.previewText.Multiline = true;
            this.previewText.Name = "previewText";
            this.previewText.ReadOnly = true;
            this.previewText.Size = new System.Drawing.Size(742, 18);
            this.previewText.TabIndex = 21;
            this.previewText.WordWrap = false;
            // 
            // previewLabel
            // 
            this.previewLabel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.previewLabel.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.previewLabel.Location = new System.Drawing.Point(27, 220);
            this.previewLabel.Margin = new System.Windows.Forms.Padding(6, 19, 20, 5);
            this.previewLabel.Name = "previewLabel";
            this.previewLabel.Size = new System.Drawing.Size(96, 18);
            this.previewLabel.TabIndex = 31;
            this.previewLabel.Text = "미리보기";
            this.previewLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnPreview
            // 
            this.BtnPreview.BackColor = System.Drawing.SystemColors.ControlLight;
            this.BtnPreview.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BtnPreview.Location = new System.Drawing.Point(145, 255);
            this.BtnPreview.Margin = new System.Windows.Forms.Padding(3, 21, 14, 3);
            this.BtnPreview.Name = "BtnPreview";
            this.BtnPreview.Size = new System.Drawing.Size(345, 27);
            this.BtnPreview.TabIndex = 12;
            this.BtnPreview.Text = "미리보기";
            this.BtnPreview.UseVisualStyleBackColor = false;
            this.BtnPreview.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBoxNumber
            // 
            this.comboBoxNumber.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxNumber.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBoxNumber.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBoxNumber.FormattingEnabled = true;
            this.comboBoxNumber.Location = new System.Drawing.Point(148, 15);
            this.comboBoxNumber.Margin = new System.Windows.Forms.Padding(6, 2, 20, 2);
            this.comboBoxNumber.Name = "comboBoxNumber";
            this.comboBoxNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxNumber.Size = new System.Drawing.Size(70, 22);
            this.comboBoxNumber.TabIndex = 25;
            this.comboBoxNumber.SelectedIndexChanged += new System.EventHandler(this.comboBoxNumber_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(27, 47);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 21, 20, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 28);
            this.label7.TabIndex = 23;
            this.label7.Text = "생성일자";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CreationDate
            // 
            this.CreationDate.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.CreationDate.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CreationDate.Location = new System.Drawing.Point(149, 47);
            this.CreationDate.Margin = new System.Windows.Forms.Padding(6, 21, 6, 5);
            this.CreationDate.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.CreationDate.Name = "CreationDate";
            this.CreationDate.Size = new System.Drawing.Size(291, 24);
            this.CreationDate.TabIndex = 4;
            this.CreationDate.Value = new System.DateTime(2020, 1, 10, 0, 0, 0, 0);
            this.CreationDate.ValueChanged += new System.EventHandler(this.CreationDate_ValueChanged);
            // 
            // pBoxLineColor
            // 
            this.pBoxLineColor.BackColor = System.Drawing.SystemColors.Control;
            this.pBoxLineColor.Location = new System.Drawing.Point(146, 157);
            this.pBoxLineColor.Name = "pBoxLineColor";
            this.pBoxLineColor.Size = new System.Drawing.Size(135, 24);
            this.pBoxLineColor.TabIndex = 26;
            this.pBoxLineColor.TabStop = false;
            // 
            // pBoxFrontColor
            // 
            this.pBoxFrontColor.Location = new System.Drawing.Point(427, 157);
            this.pBoxFrontColor.Name = "pBoxFrontColor";
            this.pBoxFrontColor.Size = new System.Drawing.Size(135, 24);
            this.pBoxFrontColor.TabIndex = 27;
            this.pBoxFrontColor.TabStop = false;
            // 
            // pBoxLockColor
            // 
            this.pBoxLockColor.Location = new System.Drawing.Point(752, 157);
            this.pBoxLockColor.Name = "pBoxLockColor";
            this.pBoxLockColor.Size = new System.Drawing.Size(135, 24);
            this.pBoxLockColor.TabIndex = 28;
            this.pBoxLockColor.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(894, 287);
            this.Controls.Add(this.pBoxLockColor);
            this.Controls.Add(this.pBoxFrontColor);
            this.Controls.Add(this.pBoxLineColor);
            this.Controls.Add(this.comboBoxNumber);
            this.Controls.Add(this.BtnPreview);
            this.Controls.Add(this.previewLabel);
            this.Controls.Add(this.previewText);
            this.Controls.Add(this.lblColor3);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblColor2);
            this.Controls.Add(this.CreationDate);
            this.Controls.Add(this.expiryDate);
            this.Controls.Add(this.filename);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.s);
            this.Controls.Add(this.lblColor1);
            this.Controls.Add(this.url);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lineAddColor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.frontAddColor);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lockAddColor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.price);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxdn);
            this.Font = new System.Drawing.Font("굴림", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(20, 43, 20, 15);
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "RenameImage";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLineColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxFrontColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLockColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox filename;
        private System.Windows.Forms.TextBox s;
        private System.Windows.Forms.TextBox url;
        private System.Windows.Forms.TextBox lineAddColor;
        private System.Windows.Forms.TextBox price;
        private System.Windows.Forms.ComboBox comboBoxdn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblColor1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker expiryDate;
        private System.Windows.Forms.Label lblColor2;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label lblColor3;
        private System.Windows.Forms.TextBox lockAddColor;
        private System.Windows.Forms.TextBox frontAddColor;
        private System.Windows.Forms.TextBox previewText;
        private System.Windows.Forms.Label previewLabel;
        private System.Windows.Forms.Button BtnPreview;
        private System.Windows.Forms.ComboBox comboBoxNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker CreationDate;
        private System.Windows.Forms.PictureBox pBoxLineColor;
        private System.Windows.Forms.PictureBox pBoxFrontColor;
        private System.Windows.Forms.PictureBox pBoxLockColor;
    }
}

